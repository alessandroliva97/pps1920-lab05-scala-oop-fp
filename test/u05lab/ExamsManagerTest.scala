package u05lab

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u05lab.code.{ExamResultFactory, ExamsManager, Kind}

import scala.collection.immutable.HashSet

class ExamsManagerTest {

  @Test
  def testExamResultBasicBehaviour(): Unit = {
    assertEquals(Kind.FAILED, ExamResultFactory.failed().kind)
    assertFalse(ExamResultFactory.failed().evaluation.isDefined)
    assertFalse(ExamResultFactory.failed().cumLaude)
    assertEquals("FAILED", ExamResultFactory.failed().toString)

    assertEquals(Kind.RETIRED, ExamResultFactory.retired().kind)
    assertFalse(ExamResultFactory.retired().evaluation.isDefined)
    assertFalse(ExamResultFactory.retired().cumLaude)
    assertEquals("RETIRED", ExamResultFactory.retired().toString)

    assertEquals(Kind.SUCCEEDED, ExamResultFactory.succeeded(25).kind)
    assertEquals(Option.apply(25), ExamResultFactory.succeeded(25).evaluation)
    assertFalse(ExamResultFactory.succeeded(25).cumLaude)
    assertEquals("SUCCEEDED(25)", ExamResultFactory.succeeded(25).toString)

    assertEquals(Kind.SUCCEEDED, ExamResultFactory.succeededCumLaude().kind)
    assertEquals(Option.apply(30), ExamResultFactory.succeededCumLaude().evaluation)
    assertTrue(ExamResultFactory.succeededCumLaude().cumLaude)
    assertEquals("SUCCEEDED(30L)", ExamResultFactory.succeededCumLaude().toString)
  }

  @Test
  def optionalTestEvaluationCanBeGreaterThan30(): Unit = {
    assertThrows(classOf[IllegalArgumentException], () => ExamResultFactory.succeeded(32))
  }

  @Test
  def optionalTestEvaluationCanBeSmallerThan18(): Unit = {
    assertThrows(classOf[IllegalArgumentException], () => ExamResultFactory.succeeded(15))
  }

  private def prepareExams(): ExamsManager = {
    val em = ExamsManager.apply()
    em.createNewCall("gennaio")
    em.createNewCall("febbraio")
    em.createNewCall("marzo")

    em.addStudentResult("gennaio", "rossi", ExamResultFactory.failed())
    em.addStudentResult("gennaio", "bianchi", ExamResultFactory.retired())
    em.addStudentResult("gennaio", "verdi", ExamResultFactory.succeeded(28))
    em.addStudentResult("gennaio", "neri", ExamResultFactory.succeededCumLaude())

    em.addStudentResult("febbraio", "rossi", ExamResultFactory.failed())
    em.addStudentResult("febbraio", "bianchi", ExamResultFactory.succeeded(20))
    em.addStudentResult("febbraio", "verdi", ExamResultFactory.succeeded(30))

    em.addStudentResult("marzo", "rossi", ExamResultFactory.succeeded(25))
    em.addStudentResult("marzo", "bianchi", ExamResultFactory.succeeded(25))
    em.addStudentResult("marzo", "viola", ExamResultFactory.failed())

    em
  }

  @Test
  def testExamsManagement(): Unit = {
    val em = prepareExams()

    assertEquals(HashSet("rossi", "bianchi", "verdi", "neri"), em.getAllStudentsFromCall("gennaio"))
    assertEquals(HashSet("rossi", "bianchi", "viola"), em.getAllStudentsFromCall("marzo"))

    assertEquals(2, em.getEvaluationMapFromCall("gennaio").size)
    assertEquals(28, em.getEvaluationMapFromCall("gennaio")("verdi"))
    assertEquals(30, em.getEvaluationMapFromCall("gennaio")("neri"))

    assertEquals(2, em.getEvaluationMapFromCall("febbraio").size)
    assertEquals(20, em.getEvaluationMapFromCall("febbraio")("bianchi"))
    assertEquals(30, em.getEvaluationMapFromCall("febbraio")("verdi"))

    assertEquals(3, em.getResultMapFromStudent("rossi").size)
    assertEquals("FAILED", em.getResultMapFromStudent("rossi")("gennaio"))
    assertEquals("FAILED", em.getResultMapFromStudent("rossi")("febbraio"))
    assertEquals("SUCCEEDED(25)", em.getResultMapFromStudent("rossi")("marzo"))

    assertEquals(3, em.getResultMapFromStudent("bianchi").size)
    assertEquals("RETIRED", em.getResultMapFromStudent("bianchi")("gennaio"))
    assertEquals("SUCCEEDED(20)", em.getResultMapFromStudent("bianchi")("febbraio"))
    assertEquals("SUCCEEDED(25)", em.getResultMapFromStudent("bianchi")("marzo"))

    assertEquals(1, em.getResultMapFromStudent("neri").size)
    assertEquals("SUCCEEDED(30L)", em.getResultMapFromStudent("neri")("gennaio"))
  }

  @Test
  def optionalTestExamManagement(): Unit = {
    val em = prepareExams()

    assertEquals(Option.apply(25), em.getBestResultFromStudent("rossi"))
    assertEquals(Option.apply(25), em.getBestResultFromStudent("bianchi"))
    assertEquals(Option.apply(30), em.getBestResultFromStudent("neri"))
    assertEquals(Option.empty, em.getBestResultFromStudent("viola"))
  }

  @Test
  def optionalTestCantCreateACallTwice(): Unit = {
    val em = prepareExams()
    assertThrows(classOf[IllegalArgumentException], () => em.createNewCall("marzo"))
  }

  @Test
  def optionalTestCantRegisterAnEvaluationTwice(): Unit = {
    val em = prepareExams()
    assertThrows(classOf[IllegalArgumentException], () => em.addStudentResult("gennaio", "verdi", ExamResultFactory.failed()))
  }



}
