package u05lab.code

import scala.collection.immutable.HashMap

object ExamsManager {
  def apply(): ExamsManager = ExamsManagerImpl()
}

sealed trait Kind

object Kind {

  val RETIRED: Kind = Retired()
  val FAILED: Kind = Failed()
  val SUCCEEDED: Kind = Succeeded()

  private case class Retired() extends Kind {
    override def toString = "RETIRED"
  }

  private case class Failed() extends Kind {
    override def toString = "FAILED"
  }

  private case class Succeeded() extends Kind {
    override def toString = "SUCCEEDED"
  }

}

sealed trait ExamResult {
  val kind: Kind
  val evaluation: Option[Int]
  val cumLaude: Boolean

  override def toString: String = kind.toString
}

object ExamResultFactory {

  private class AbstractExamResult(val kind: Kind) extends ExamResult {
    override val evaluation: Option[Int] = Option.empty
    override val cumLaude: Boolean = false
  }

  private class AbstractSuccessExamResult(eval: Int) extends AbstractExamResult(Kind.SUCCEEDED) {
    if (eval < 18 || eval > 30)
      throw new IllegalArgumentException
    override val evaluation: Option[Int] = Option(eval)
  }

  def failed(): ExamResult = new AbstractExamResult(Kind.FAILED)

  def retired(): ExamResult = new AbstractExamResult(Kind.RETIRED)

  def succeeded(eval: Int): ExamResult = new AbstractSuccessExamResult(eval) {
    override def toString: String = super.toString + "(" + eval + ")"
  }

  def succeededCumLaude(): ExamResult = new AbstractSuccessExamResult(30) {
    override val cumLaude: Boolean = true

    override def toString: String = super.toString + "(30L)"
  }

}

sealed trait ExamsManager {
  def createNewCall(call: String): Unit

  def addStudentResult(call: String, student: String, result: ExamResult): Unit

  def getAllStudentsFromCall(call: String): Set[String]

  def getEvaluationMapFromCall(call: String): Map[String, Int]

  def getResultMapFromStudent(student: String): Map[String, String]

  def getBestResultFromStudent(student: String): Option[Int]
}

case class ExamsManagerImpl() extends ExamsManager {
  private var map: Map[String, Map[String, ExamResult]] = new HashMap()

  private def checkArgument(condition: Boolean): Unit = {
    if (!condition) throw new IllegalArgumentException
  }

  override def createNewCall(call: String): Unit = {
    checkArgument(!map.contains(call))
    map += call -> HashMap()
  }

  override def addStudentResult(call: String, student: String, result: ExamResult): Unit = {
    checkArgument(map.contains(call))
    checkArgument(!map(call).contains(student))
    map += (call -> (map(call) + (student -> result)))
  }

  override def getAllStudentsFromCall(call: String): Set[String] = {
    checkArgument(map.contains(call))
    map(call).keySet
  }

  override def getEvaluationMapFromCall(call: String): Map[String, Int] = {
    checkArgument(map.contains(call))
    map(call).filter(_._2.evaluation.isDefined).mapValues(_.evaluation.get)
  }

  override def getResultMapFromStudent(student: String): Map[String, String] = map.filter(_._2.contains(student)).mapValues(_.apply(student).toString)

  override def getBestResultFromStudent(student: String): Option[Int] = map.toSeq.flatMap(t => t._2.toSeq).filter(_._1 == student).map(_._2).filter(_.evaluation.isDefined).map(_.evaluation.get).reduceOption(math.max)

}