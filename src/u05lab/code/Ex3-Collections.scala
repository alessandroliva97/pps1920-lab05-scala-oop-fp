package u05lab.code

import java.util.concurrent.TimeUnit

import scala.collection.mutable.{ArrayBuffer, ListBuffer, Set => MutableSet}
import scala.concurrent.duration.FiniteDuration

object PerformanceUtils {
  case class MeasurementResults[T](result: T, duration: FiniteDuration) extends Ordered[MeasurementResults[_]] {
    override def compare(that: MeasurementResults[_]): Int = duration.toNanos.compareTo(that.duration.toNanos)
  }

  def measure[T](msg: String)(expr: => T): MeasurementResults[T] = {
    val startTime = System.nanoTime()
    val res = expr
    val duration = FiniteDuration(System.nanoTime()-startTime, TimeUnit.NANOSECONDS)
    if(!msg.isEmpty) println(msg + " -- " + duration.toNanos + " nanos; " + duration.toMillis + "ms")
    MeasurementResults(res, duration)
  }

  def measure[T](expr: => T): MeasurementResults[T] = measure("")(expr)
}


object CollectionsTest extends App {

  /* Linear sequences: List, ListBuffer */
  var list = scala.collection.immutable.List(0, 1, 2)
  println(list.head, list.size)
  list = list :+ 3
  list = list drop 1
  println(list)

  var listbuffer = ListBuffer(0, 1, 2)
  println(listbuffer.head, listbuffer.size)
  listbuffer = listbuffer :+ 3
  listbuffer = listbuffer drop 1
  println(listbuffer)

  /* Indexed sequences: Vector, Array, ArrayBuffer */

  var vector = Vector(0, 1, 2)
  println(vector.head, vector.size)
  vector = vector :+ 3
  vector = vector drop 1
  println(vector)

  var array = Array(0, 1, 2)
  println(array.head, array.length)
  array = array :+ 3
  array = array drop 1
  println("Array(" + array.mkString(", ") + ")")

  var arrayBuffer = ArrayBuffer(0, 1, 2)
  println(arrayBuffer.head, arrayBuffer.size)
  arrayBuffer = arrayBuffer :+ 3
  arrayBuffer = arrayBuffer drop 1
  println(arrayBuffer)

  /* Sets */

  var set = Set(0, 1, 2)
  println(set.head, set.size)
  set += 3
  set = set drop 1
  println(set)

  var mutableSet = MutableSet(0, 1, 2)
  println(mutableSet.head, mutableSet.size)
  mutableSet += 3
  mutableSet = mutableSet drop 1
  println("Mutable" + mutableSet)

  /* Maps */

  var map = Map[Int, Int]((0, 0), (1, 1), (2, 2))
  println(map.head, map.size)
  map += 3 -> 3
  map = map drop 1
  println(map)

  var mutableMap = Map[Int, Int]((0, 0), (1, 1), (2, 2))
  println(mutableMap.head, mutableMap.size)
  mutableMap += 3 -> 3
  mutableMap = mutableMap drop 1
  println("Mutable" + mutableMap)

  /* Comparison */
  import PerformanceUtils._
  val lst = (1 to 1000000).toList
  val vec = (1 to 1000000).toVector
  assert( measure("lst last"){ lst.last } > measure("vec last"){ vec.last } )
}